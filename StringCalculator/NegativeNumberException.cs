﻿using System;
using System.Collections.Generic;

namespace StringCalc
{
    public class NegativeNumberException : Exception
    {        
        public NegativeNumberException() : base() {}

        public NegativeNumberException(int number) : base(BuildMessage(number)) { }

        public NegativeNumberException(int number, Exception ex) : base(BuildMessage(number), ex) { }

        public NegativeNumberException(IList<int> numbers) : base(BuildMessage(numbers)) { }

        public NegativeNumberException(IList<int> numbers, Exception ex) : base(BuildMessage(numbers)) { }
        
        private static string BuildMessage(int number)
        {
            return $"negatives not allowed, {number} was given";
        }

        private static string BuildMessage(IList<int> numbers)
        {
            return $"negatives not allowed, {string.Join(',', numbers)} were given";
        }
    }
}
