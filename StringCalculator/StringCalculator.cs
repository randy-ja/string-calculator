﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace StringCalc
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            // if the string is empty, return 0
            if (numbers.Length == 0)
            {
                return 0;
            }

            // default separators
            List<string> separators = new List<string>
            {
                "\n",
                ","
            };

            List<string> customDelimiters = ParseDelimiters(numbers, out int endOfDelimiter);

            separators.AddRange(customDelimiters);

            // if any custom delimiters were found, remove them before splitting
            if (customDelimiters.Count > 0)
            {
                // remove characters from the beginning of the string to the \n after the last delimiter
                numbers = numbers.Remove(0, endOfDelimiter);
            }

            string[] stringArray = numbers.Split(separators.ToArray(), StringSplitOptions.None);

            IList<int> negativesFound = new List<int>();

            int sum = 0;
            Array.ForEach(stringArray, element => {
                int value = int.Parse(element);
                if(value < 0)
                {
                    negativesFound.Add(value);
                }

                // only add numbers less than or equal to 1000
                if (value <= 1000)
                {
                    sum += value;
                }
            });

            // if any negative numbers were found, throw an exception
            if(negativesFound.Count > 0)
            {
                throw new NegativeNumberException(negativesFound);
            }
            return sum;
        }

        private List<string> ParseDelimiters(string numbers, out int endOfDelimiter)
        {
            List<string> delimiters = new List<string>();
            endOfDelimiter = 0;
            // check if any delimiter was supplied
            if (numbers.IndexOf("//") == 0)
            {
                // check if the delimiter is surrounded by []
                if (numbers[2].Equals('['))
                {

                    int end = numbers.IndexOf("]\n") + 2; // add 2 to move past ]\n
                    endOfDelimiter = end;

                    // regex to find all instances of text betwen []
                    Regex regex = new Regex(@"\[([^\[\]]+)\]*");
                    delimiters = regex.Matches(numbers)
                            .Cast<Match>()
                            .Select(m => m.Groups[1].Value)
                            .ToList();
                }
                else
                {
                    // the third item in the string is the delimiter
                    delimiters.Add(numbers[2].ToString());
                    // the fourth item in the string is \n
                    endOfDelimiter = 4;
                }
            }
            return delimiters;
        }
    }
}
