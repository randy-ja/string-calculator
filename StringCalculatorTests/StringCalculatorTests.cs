using NUnit.Framework;
using StringCalc;
using System;

namespace Tests
{
    public class Tests
    {
        StringCalculator calculator;

        [SetUp]
        public void Setup()
        {
            calculator = new StringCalculator();
        }

        [Test]
        public void One_Empty_String_Returns_0()
        {
            int result = calculator.Add(string.Empty);

            Assert.That(result, Is.EqualTo(0));
        }


        [TestCase("3", 3)]
        [TestCase("83", 83)]
        [TestCase("349", 349)]
        public void One_Number_Returns_Same_Number(string number, int expected)
        {
            int result = calculator.Add(number);

            Assert.That(result, Is.EqualTo(expected));
        }


        [TestCase("4,5", 9)]
        [TestCase("19,48", 67)]
        [TestCase("0,10", 10)]
        public void Two_Numbers_Are_Added(string numbers, int expected)
        {
            int result = calculator.Add(numbers);

            Assert.That(result, Is.EqualTo(expected));
        }

        
        [TestCase("1,2,3,4", 10)]
        [TestCase("40,10,30,60,70,90,100", 400)]
        [TestCase("2,3,5,7,11,13,17,19", 77)]
        public void Multiple_Numbers_Are_Added(string numbers, int expected)
        {
            int result = calculator.Add(numbers);

            Assert.That(result, Is.EqualTo(expected));
        }


        [TestCase("3\n4,5", 12)]
        [TestCase("10\n20", 30)]
        [TestCase("5\n10\n50,2\n1", 68)]
        public void Numbers_Separated_By_Newline_Are_Added(string numbers, int expected)
        {
            int result = calculator.Add(numbers);

            Assert.That(result, Is.EqualTo(expected));
        }


        [TestCase("//;\n1;2", 3)]
        [TestCase("//|\n6|8|2|10", 26)]
        [TestCase("//|\n5|10,15,10\n20", 60)]
        public void Custom_Separators_Are_Supported(string numbers, int expected)
        {
            int result = calculator.Add(numbers);

            Assert.That(result, Is.EqualTo(expected));
        }


        [TestCase("5,-9,10", -9)]
        [TestCase("-3\n1\n50", -3)]
        [TestCase("//|\n20|-10|31", -10)]
        public void Negative_Numbers_Throw_Exception(string numbers, int negative)
        {
            Assert.Throws(Is.TypeOf<NegativeNumberException>()
                .And.Message.StartsWith("negatives not allowed")
                .And.Message.Contains(negative.ToString()),
                () => calculator.Add(numbers));
        }


        [TestCase("-2,-9,31", -2, -9)]
        [TestCase("38\n-8\n-95,12", -8, -95)]
        [TestCase("//|\n-4|12|-49", -4, -49)]
        public void Multiple_Negatives_Shown_In_Exception(string numbers, params int[] negatives)
        {
            Exception ex = Assert.Throws(Is.TypeOf<NegativeNumberException>(), () => calculator.Add(numbers));

            Assert.That(ex.Message.StartsWith("negatives not allowed"));
            foreach (int negative in negatives)
            {
                Assert.That(ex.Message.Contains(negative.ToString()));
            }
        }


        [TestCase("5,1005,10", 15)]
        [TestCase("20\n100000\n30", 50)]
        [TestCase("//|\n100000|40|9", 49)]
        public void Ignore_Numbers_Greater_Than_1000(string numbers, int expected)
        {
            int result = calculator.Add(numbers);

            Assert.That(result, Is.EqualTo(expected));
        }


        [TestCase("//[anylength]\n1anylength2anylength3", 6)]
        [TestCase("//[*****]\n10*****15*****5", 30)]
        [TestCase("//[###]\n5###6###8", 19)]
        public void Delimiters_Can_Be_Any_Length(string numbers, int expected)
        {
            int result = calculator.Add(numbers);

            Assert.That(result, Is.EqualTo(expected));
        }


        [TestCase("//[|][*]\n1|2*3", 6)]
        [TestCase("//[**][##]\n10##20**30##40", 100)]
        public void Multiple_Custom_Delimiters(string numbers, int expected)
        {
            int result = calculator.Add(numbers);

            Assert.That(result, Is.EqualTo(expected));
        }
    }
}